package views;

import java.util.*;

import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.transform.Transform;
import javafx.scene.transform.Translate;
import models.*;
import javafx.scene.shape.Polygon;
import javafx.scene.transform.Scale;
import javafx.stage.Stage;
import javafx.scene.layout.Pane;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.transform.Affine;

public class View implements Observer {

    /**
     * Statische Variable für Breite des Fensters
     */
    public static final int BREITE = 1000;

    /**
     * Statische Variable für die Höhe des Fensters
     */
    public static final int HOEHE = 600;

    /**
     * Fillermap Instanz
     */
    private FillerMap fillermap;

    /**
     * Stage (Fenster) Instanz
     */
    private Stage stage;

    /**
     * Instanz der Wurzelpane
     */
    private Pane root;

    /**
     * Instanz einer Pane, welche später zur Wurzelpane hinzugefügt wird
     */
    private Pane pane;

    /**
     * Anzeige, welcher Spieler gerade am Zug ist
     */
    private Text aktiverSpielerText;

    /**
     * Spielstand für Spieler 1
     */
    private Text spielstandSpieler1Text;

    /**
     * Spielstand für Spieler 2
     */
    private Text spielstandSpieler2Text;

    /**
     * Anzeige für das Spielende
     */
    private Text spielEndeText;

    /**
     * Anzeige für einen ungültigen Spielzug
     */
    private Text ungueltigerSpielzugText;

    /**
     * Umrandungsfarbe für Spieler 1
     */
    private Color umrandungsFarbeSpieler1;

    /**
     * Umrandungsfarbe für Spieler 2
     */
    private Color umrandungsFarbeSpieler2;

    /**
     * @value farbenMap: Hier findet die Abbildung eines Farben Enums (als Key) auf Farbwerte der JavaFX Color Klasse
     * (Value) statt...
     * <p>
     * Beispiel: FARBEN.A wird auf Color.AQUAMARINE abgebildet.
     * <p>
     * Die Farbenmap wird in der Funktion @see initialisiereFarbenMap() initialisiert.
     */
    private Map<FARBEN, Color> farbenMap;

    /**
     * Affine für Transformationen
     */
    private Affine affine;

    /**
    * Hier findet eine Abbildung von KeyCode (Key) auf Transform Objekte (Value) statt.
     *
     * Beispiel: der KeyCode für die Taste G wird auf eine Vergrößerung abgebildet.
     */
 	private Map<KeyCode, Transform> keyCodeMap;

    /**
     * @param fillermap Instanz der Fillermap
     * @param stage     Instanz der gewünschten Stage
     */
    public View(FillerMap fillermap, Stage stage) {
        this.fillermap = fillermap;
        this.stage = stage;
        this.root = new Pane();
        this.pane = new Pane();
        affine = new Affine();
        pane.getTransforms().add(affine); //nimmt affine Ersetzungen vor

        this.aktiverSpielerText = new Text();
        this.spielstandSpieler1Text = new Text();
        this.spielstandSpieler2Text = new Text();
        this.spielEndeText = new Text();
        this.ungueltigerSpielzugText = new Text();

        this.umrandungsFarbeSpieler1 = Color.BLACK;
        this.umrandungsFarbeSpieler2 = Color.YELLOW;

        this.initialisiereFarbenMap();
        this.initialisiereKeyCodeMap();

        fillermap.addObserver(this);

        this.zeichneRegionen();//kann auch ohne this

        root.getChildren().add(this.pane);
        root.getChildren().add(this.aktiverSpielerText);
        root.getChildren().add(this.spielstandSpieler1Text);
        root.getChildren().add(this.spielstandSpieler2Text);
        root.getChildren().add(this.spielEndeText);
        root.getChildren().add(this.ungueltigerSpielzugText);

        Scene scene = new Scene(this.root, BREITE, HOEHE);
        pane.setScaleY(-1);

        double mapcenterX = 0.5 * (this.fillermap.getMaxX() + this.fillermap.getMinX());
        double mapcenterY = 0.5 * (this.fillermap.getMaxY() + this.fillermap.getMinY());


        affine.append(new Translate(BREITE / 2 - mapcenterX, -HOEHE / 2 + mapcenterY));
        affine.append(new Scale(3, 3));

        this.markiereStartRegionen(this.fillermap.getAktuellenSpieler().getGewonneneRegions(),
                                   this.umrandungsFarbeSpieler1);

        this.markiereStartRegionen(this.fillermap.getAnderenSpieler().getGewonneneRegions(),
                                   this.umrandungsFarbeSpieler2);

        this.zeigeAktivenSpieler();
        this.zeigeSpielstandSpieler1();
        this.zeigeSpielstandSpieler2();

        this.setzeEventHandler(scene);

        this.stage.setScene(scene);
        this.stage.show();
    }

    /**
     * Wird bei jeder Aenderung am FillerMap aufgerufen.
     */
    @Override
    public void update(Observable o, Object arg) {
    }

    /**
     * Methode zum Zeichnen der Regionen.
     * <p>
     * Iteration über alle Regionen der Fillermap. Danach wird die einzelne Region gezeichnet und mit einer Zufallsfarbe
     * versehen.
     *
     * Wir iterieren über aller Regionen und zeichnen mit Hilfe von den Koordinaten
     * von der Region für jede Region ein Polygon
     */
    private void zeichneRegionen() {
        this.fillermap.getAllRegions().forEach((region -> {
            List<Double> ergebnisse = getKoordinatenList(region.getKoordinaten());
            Polygon polygon = new Polygon();
            polygon.getPoints().addAll(ergebnisse);
            polygon.setFill(this.farbenMap.get(region.getFarbe()));

            pane.getChildren().add(polygon);

            polygon.setOnMouseClicked((event) -> {
                if (fillermap.gueltigerSpielzug(region)) {
                    this.faerbeSpielstandAktuellenSpielers(this.farbenMap.get(region.getFarbe())); //Textfarbe
                    this.resetText(this.ungueltigerSpielzugText);   //damit der Text weggeht
                    Spieler jetzigerSpieler = this.fillermap.getAktuellenSpieler();
                    Color umrandungsFarbe = this.fillermap.getAktuellenSpieler() == this.fillermap.getSpieler1()
                            ? this.umrandungsFarbeSpieler1 : this.umrandungsFarbeSpieler2; //Spieler1 black, Spieler2 yellow
                    this.fillermap.macheSpielzug(region); //
                    this.zeigeAktivenSpieler();

                    //Auf die gewonenne Regionen zeichnet Poligonen in die neue Farbe drauf
                    for (Region gewonneneRegion : jetzigerSpieler.getGewonneneRegions()) {
                        List<Double> ergebnisse2 = getKoordinatenList(gewonneneRegion.getKoordinaten());
                        Polygon polygon2 = new Polygon();
                        polygon2.getPoints().addAll(ergebnisse2);
                        polygon2.setFill(this.farbenMap.get(region.getFarbe()));
                        polygon2.setStrokeWidth(0.02);
                        polygon2.setStroke(umrandungsFarbe);

                        //Wenn ein Loch drinnen ist, zeichnet ein neues Poligon drauf(die alte Farbe bleibt)
                        if (gewonneneRegion.getLoecher().size() > 0) {
                            List<Double> lochKoordinaten = getKoordinatenList(gewonneneRegion.getLoecher());

                            Polygon lochPolygon = new Polygon();
                            lochPolygon.getPoints().addAll(lochKoordinaten);
                            //das Loch wird vom Plygon ausgeschnitten
                            Shape ausgeschnitteneRegion = Shape.subtract(polygon2, lochPolygon);

                            ausgeschnitteneRegion.setFill(this.farbenMap.get(region.getFarbe()));
                            ausgeschnitteneRegion.setStrokeWidth(0.02);
                            ausgeschnitteneRegion.setStroke(umrandungsFarbe);
                            pane.getChildren().add(ausgeschnitteneRegion);
                        } else {
                            pane.getChildren().add(polygon2);
                        }
                    }
                    this.zeigeSpielstandSpieler1();
                    this.zeigeSpielstandSpieler2();
                } else {
                    this.zeigeUngueltigenSpielzug();
                }
                if (this.fillermap.spielEnde()) {
                    this.zeigeSpielEnde();
                }
            });
        }));
    }

    /**
     * Methode zur Erstellung einer Koordinatenliste, welche vom JavaFX Polygon verwendet werden kann.
     * <p>
     * Aus einer Koordinatenliste der Form [ [0.0], [1.0] ... ] wird hier eine Liste von Doublewerten der Form
     * [ 0,0, 1.0 ... ] erzeugt. Hintergrund: die .addAll() Funktion des JavaFX Polygon Objektes kann nur mit einer
     * Liste an Doublewerten umgehen.
     * <p>
     * Deswegen findet hier die Umwandlung von {@code Liste<Koordinate>}  auf  {@code Liste<Double>} statt.
     *
     * @param koordinaten Liste von Koordinaten
     * @return Liste von Doublewerten
     */
    public List<Double> getKoordinatenList(List<Koordinate> koordinaten) {
        List<Double> ergebnisse = new ArrayList<>(2 * koordinaten.size());
        koordinaten.forEach((koordinate -> {
            ergebnisse.add(koordinate.getX());
            ergebnisse.add(koordinate.getY());
        }));
        return ergebnisse;
    }


    /**
    * Methode zur Initialisierung der keyCodeMap.*/
 	private void initialisiereKeyCodeMap() {
        this.keyCodeMap = new HashMap<>();
        this.keyCodeMap.put(KeyCode.G, new Scale(1.1, 1.1));
        this.keyCodeMap.put(KeyCode.K, new Scale(0.9, 0.9));
        this.keyCodeMap.put(KeyCode.LEFT, new Translate(5, 0.0));
        this.keyCodeMap.put(KeyCode.RIGHT, new Translate(-5, 0.0));
        this.keyCodeMap.put(KeyCode.UP, new Translate(0, -5));
        this.keyCodeMap.put(KeyCode.DOWN, new Translate(0, 5));

    }

    /**
     * Methode zur Initialisierung der Farbenmap.
     * <p>
     * Hierbei wird ein Mapping von {@code FARBEN} auf ein JavaFX {@code Color} Objekt vorgenommen.
     */
    private void initialisiereFarbenMap() {
        this.farbenMap = new HashMap<>();
        farbenMap.put(FARBEN.A, Color.INDIANRED);
        farbenMap.put(FARBEN.B, Color.TOMATO);
        farbenMap.put(FARBEN.C, Color.DARKSEAGREEN);
        farbenMap.put(FARBEN.D, Color.CRIMSON);
        farbenMap.put(FARBEN.E, Color.DARKGREEN);
        farbenMap.put(FARBEN.F, Color.DIMGREY);
        farbenMap.put(FARBEN.G, Color.SEAGREEN);

        /*farbenMap.put(FARBEN.A, Color.LAVENDER);
        farbenMap.put(FARBEN.B, Color.CADETBLUE);
        farbenMap.put(FARBEN.C, Color.LIGHTBLUE);
        farbenMap.put(FARBEN.D, Color.THISTLE);
        farbenMap.put(FARBEN.E, Color.PEACHPUFF);
        farbenMap.put(FARBEN.F, Color.LIGHTSLATEGREY);
        farbenMap.put(FARBEN.G, Color.PALETURQUOISE);
        */
    }

    /**
     * Methode zur Markierung der Startregionen
     *
     * Es werden hier ebenfalls benachbarte Regionen als Startregionen markiert, wenn die benachbarten Regionen
     * diesselbe Farbe haben.
     *
     * @param gewonneneRegions Liste an gewonnenen Regionen
     * @param color Einzufärbende Farbe
     */
    private void markiereStartRegionen(Set<Region> gewonneneRegions, Color color) {
        gewonneneRegions.forEach((region -> {
            this.markiereRegion(region, color);
        }));
    }

    /**
     * Versieht eine Region mit einer gewünschten Farbe
     * @param region Einzufärbende Region
     * @param color Gewünschte Farbe zur Einfärbung
     */
    private void markiereRegion(Region region, Color color) {
        List<Double> ergebnisse = getKoordinatenList(region.getKoordinaten());
        Polygon polygon = new Polygon();
        polygon.getPoints().addAll(ergebnisse);
        polygon.setFill(this.farbenMap.get(region.getFarbe()));
        polygon.setStrokeWidth(0.02);
        polygon.setStroke(color);
        this.pane.getChildren().add(polygon);
    }

    /**
     * Registriert diverse Eventhandler für die aktuelle Szene
     * @param scene Aktuelle Szene
     */
    private void setzeEventHandler(Scene scene) {
        scene.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            affine.append(this.keyCodeMap.get(event.getCode()));
            KeyCode code = event.getCode();
            /*switch (code) {
                case G:
                    affine.append(new Scale(1.1, 1.1));
                    break;
                case K:
                    affine.append(new Scale(0.9, 0.9));
                    break;
                case LEFT:
                    affine.append(new Translate(2.5, 0.0));
                    break;
                case RIGHT:
                    affine.append(new Translate(-2.5, 0.0));
                    break;
                case UP:
                    affine.append(new Translate(0, -2.5));
                    break;
                case DOWN:
                    affine.append(new Translate(0, 2.5));
                    break;
            }*/
        });
    }

    /**
     * Zeigt den aktiven Spieler (als Textfeld) an
     */
    private void zeigeAktivenSpieler() {
        int aktiverSpieler = (this.fillermap.getSpieler1().getistJetztDran()) ? 1 : 2;
        this.zeichneText(this.aktiverSpielerText, 0, 20, "Spieler " + aktiverSpieler + " ist an der Reihe",
                Font.font("Verdana", FontWeight.BOLD, 12));

    }

    /**
     * Zeigt den aktuellen Spielstand des ersten Spielers an
     */
    private void zeigeSpielstandSpieler1() {
        this.zeichneText(this.spielstandSpieler1Text, 200, 20,
                "Erster Spieler: " + (this.fillermap.getSpieler1().getSpielstand() + 1),
                Font.font("Verdana", FontWeight.BOLD, 12));
    }

    /**
     * Zeigt den aktuellen Spielstand des zweiten Spielers an
     */
    private void zeigeSpielstandSpieler2() {
        this.zeichneText(this.spielstandSpieler2Text, 400, 20,
                "Zweiter Spieler: " + (this.fillermap.getSpieler2().getSpielstand() + 1),
                Font.font("Verdana", FontWeight.BOLD, 12));
    }

    /**
     * Im Falle von einem Spielende wird ausgegeben, welcher Spieler gewonnen hat
     */
    private void zeigeSpielEnde() {
        int sieger = this.fillermap.getSpieler1().getGewonneneRegions().size()
                > this.fillermap.getSpieler2().getGewonneneRegions().size() ? 1 : 2;
        this.zeichneText(this.spielEndeText, 20, this.HOEHE / 6, "Spieler " + sieger + " hat gewonnen!",
                Font.font("Verdana", FontWeight.BOLD, 32));
    }

    /**
     * Im Falle eines ungültigen Spielzugs wird ein Hinweis angezeigt
     */
    private void zeigeUngueltigenSpielzug() {
        int aktiverSpieler = this.fillermap.getSpieler1().getistJetztDran() ? 1 : 2;
        this.zeichneText(this.ungueltigerSpielzugText, 0, this.HOEHE / 8,
                "Ungültiger Spielzug von Spieler " + aktiverSpieler,
                Font.font("Verdana", FontWeight.BOLD, 16));
    }

    /**
     * Die Spielstandsanzeige für den aktiven Spieler wird in der aktuellen Spielfarbe gefärbt
     * @param color Aktuelle Spielfarbe
     */
    private void faerbeSpielstandAktuellenSpielers(Color color) {
        Text aktuellerSpielstandText = this.fillermap.getSpieler1().getistJetztDran()
                ? this.spielstandSpieler1Text : this.spielstandSpieler2Text;

        aktuellerSpielstandText.setStroke(color);
    }

    /**
     * Zeichnet einen gewünschten Text auf die Szene
     * @param textObjekt Textobjekt, dessen Text gesetzt werden soll
     * @param x x- Koordinate
     * @param y y- Koordinate
     * @param text Zu setzender Text
     * @param font Gewünschte Schriftart
     */
    private void zeichneText(Text textObjekt, double x, double y, String text, Font font) {
        textObjekt.setX(x);
        textObjekt.setY(y);
        textObjekt.setText(text);
        textObjekt.setFont(font);
    }

    /**
     * Setzt für ein TextObjekt den anzuzeigenden Textinhalt auf einen leeren String
     * @param textObjekt Textobjekt
     */
    private void resetText(Text textObjekt) {
        textObjekt.setText("");
    }
}