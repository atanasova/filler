package models;

//import java.awt.Color;

import java.util.*;

import parser.JSONParser;

/**
 * Created by Admin on 20/11/2016.
 */
public class FillerMap extends Observable {
    ArrayList<Region> regionList;
    private double maxX;
    private double minY;
    private double minX;
    private double maxY;
    private Spieler spieler1;
    private Spieler spieler2;

    public FillerMap() {
        regionList = new ArrayList<>();
        regionList.addAll(JSONParser.getRegions(JSONParser.HEXAGONS)); // Hier werden die Daten in regionList eingelesen (COUNTRIES, HEXAGONE, LANDKREISE & RECTANGLES)
        System.out.println(regionList.size());
        spieler1 = new Spieler(true); //istAlsErtsesDran = true;
        spieler2 = new Spieler(false); // istAlsErstesDran = false;
        rechneRandKoordinaten(); // errechnet die min und max Koordinaten der Regions aus
        zufallsFarbe(); // fügt jeder Region eine Zufallsfarbe hinzu
        nachbarnBestimmen(); //jede Region kennt eine Liste von Nachbarn

        //System.out.print("minx" + minX + "maxx" + maxX + "minY" + minY + "maxY" + maxY);
        spieler1.setStartRegion(this.regionList); // Zufallsstartregion für spieler wird aus regionList bestimmt
        spieler2.setStartRegion(this.regionList);
        spieler1.getGewonneneRegions().add(spieler1.getStartRegion()); // Startregion wird den Gewonnenenregion hinzugefügt
        spieler2.getGewonneneRegions().add(spieler2.getStartRegion());
        spieler1.setFarbe(spieler1.getStartRegion().getFarbe()); // Farbe von Startregion
        spieler2.setFarbe(spieler2.getStartRegion().getFarbe());

        //Spieler 1 und 2 bekommen unterschiedliche Startregionen/Farbe
        while (spieler1.getFarbe() == spieler2.getFarbe()) {
            zufallsFarbe();
        }
        while (spieler1.getStartRegion() == spieler2.getStartRegion()) {
            spieler2.setStartRegion(this.regionList);
        }
        //Simulation eines Spielzuges, damit die anderen gleichfarbigen Nachbarregions den gewonnenen Regions hinzugefügt werden
        this.macheSpielzug(spieler1.getStartRegion());
        this.macheSpielzug(spieler2.getStartRegion());
    }

    public void rechneRandKoordinaten() {
        Koordinate first = regionList.get(0).getKoordinaten().get(0);
        minX = first.getX();
        maxX = first.getX();
        minY = first.getY();
        maxY = first.getY();
        // Grenzkoordinaten suchen
        for (Region region : regionList) {
            for (Koordinate koordinate : region.getKoordinaten()) {
                double x = koordinate.getX();
                double y = koordinate.getY();
                if (x < minX) {
                    minX = x;
                }
                if (x > maxX) {
                    maxX = x;
                }
                if (y < minY) {
                    minY = y;
                }
                if (y > maxY) {
                    maxY = y;
                }
            }
        }

    }

    public void zufallsFarbe() {

        for (Region region : regionList) {
            region.setFarbe(FARBEN.randomColor());
        }

    }

    public void nachbarnBestimmen() {
        for (Region region1 : regionList) {
            for (Region region2 : regionList) {
                if (region1 != region2) {
                    if (region1.istNachbar(region1, region2) == true) {
                        region1.getNachbarn().add(region2);
                    }
                }
            }

        }
    }
    /**
     * @param angeklickteRegion Verwendet folgende Methoden, um Spielzug umzusetzen
     * @see Region#getGleichfarbigeNachbarn(FARBEN)
     * @see Region#setFarbe(FARBEN)
     * @see Spieler#setFarbe(FARBEN)
     * @see Spieler#setIstJetztDran(boolean)
     * @see Spieler#setSpielstand(int)
     * @see Spieler#getGewonneneRegions()
     */
    public void macheSpielzug(Region angeklickteRegion) {
        FARBEN angeklickteFarbe = angeklickteRegion.getFarbe();
        System.out.println("" + angeklickteFarbe);

        // felder werden auf Nachbarschaft und Farbe geprüft um sie einzufärben
        Set<Region> toDo = new HashSet<>();
        Set<Region> zwischenspeicher = new HashSet<>();
        Set<Region> erledigt = new HashSet<>();
        toDo.addAll(this.getAktuellenSpieler().getGewonneneRegions());
        //angeklickte Region und bisher gewonnen Regions müssen Nachbarn und Farben geprüft werden, um einzufärbende Regions zu finden
        toDo.addAll(angeklickteRegion.getGleichfarbigeNachbarn(angeklickteFarbe));
        erledigt.add(angeklickteRegion);
        while (toDo.size() != 0) {
            Region betrachteteRegion = toDo.iterator().next();
            toDo.remove(betrachteteRegion);
            zwischenspeicher.addAll(betrachteteRegion.getGleichfarbigeNachbarn(angeklickteFarbe));
            zwischenspeicher.removeAll(erledigt);
            toDo.addAll(zwischenspeicher);
            zwischenspeicher.clear();
            erledigt.add(betrachteteRegion);
        }
        // In erledigt sind alle gewonnenen Regions
        // Diese werden nun zu den gewonnenenRegions des Spielers hinzugefügt
        this.getAktuellenSpieler().getGewonneneRegions().addAll(erledigt);
        //alle gewonnen Regions werden in der neuen Farbe eingefärbt
        for (Region einzufaerbendeRegion : this.getAktuellenSpieler().getGewonneneRegions()) {
            einzufaerbendeRegion.setFarbe(angeklickteFarbe);
        }
        // Spieler merkt sich die neue Farbe
        this.getAktuellenSpieler().setFarbe(angeklickteFarbe);
        // spielstand von aktuellem Spieler wird gesetzt
        this.getAktuellenSpieler().setSpielstand(this.getAktuellenSpieler().getGewonneneRegions().size() - 1);
        // istJetztDran wird bei aktuellem Spieler false und beim anderen true
        Spieler spielerDerGezogenHat = this.getAktuellenSpieler();
        // Der Spieler wird gewechselt
        spieler1.setIstJetztDran(spieler1 == spielerDerGezogenHat ? false : true);
        spieler2.setIstJetztDran(spieler2 == spielerDerGezogenHat ? false : true);
    }

    public boolean gueltigerSpielzug(Region region) {
        FARBEN angeklickteFarbe = region.getFarbe();
        if (angeklickteFarbe != spieler1.getFarbe() && angeklickteFarbe != spieler2.getFarbe() &&
                this.getAktuellenSpieler().gewonneneRegionsNachbar(region)) {
            System.out.println("gültig!");
            return true;
        }
        System.out.println("ungültig!");
        return false;
    }
    //Das Spiel ist vorbei wenn es keine Nachbarn gibt, mit einer Farbe, die frei ist
    public boolean spielEnde() {
        for (Region region : this.getAktuellenSpieler().getGewonneneRegions()) {
            for (Region nachbar : region.getNachbarn()) {
                if (nachbar.getFarbe() != spieler1.getFarbe() && nachbar.getFarbe() != spieler2.getFarbe()) {
                    return false;
                }
            }

        }
        System.out.println("Das Spielende ist endlich erreicht!");
        return true;

    }

    public List<Region> getAllRegions() {
        return regionList;
    }

    public Spieler getAktuellenSpieler() {
        return (spieler1.getistJetztDran() ? spieler1 : spieler2);
    }

    public Spieler getAnderenSpieler() {
        return (!spieler1.getistJetztDran() ? spieler1 : spieler2);
    }

    public Spieler getSpieler1() {
        return spieler1;
    }

    public Spieler getSpieler2() {
        return spieler2;
    }

    public double getMaxX() {
        return maxX;
    }

    public double getMinX() {
        return minX;
    }

    public double getMaxY() {
        return maxY;
    }

    public double getMinY() {
        return minY;
    }

}


