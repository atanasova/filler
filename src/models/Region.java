package models;

//import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import parser.RegionJSON;

/**
 * Created by Admin on 20/11/2016.
 */
public class Region {
    private String name;
	private List<Koordinate> koordinaten;
	private List<Koordinate> loecher;
	private FARBEN farbe;
	private Set<Region> nachbarn;

	/**
	 * Konstruktor
	 */
    public Region() {
        koordinaten = new ArrayList<>();
        loecher = new ArrayList<>();
		nachbarn = new HashSet<>();
    }

	/**
	 * Konstruktor
	 * @param regionJSON	Die urspruengliche Region, die die gleichen Attribute hat wie in der JSON-Datei
	 */
	public Region(RegionJSON regionJSON) {
		this();
		this.name = regionJSON.getName();

		double[][] boundary = regionJSON.getBoundary();
		if (boundary != null) {
			for (int i = 0; i < boundary.length; i++) {
				koordinaten.add(new Koordinate(boundary[i][0], boundary[i][1]));
			}
		}


		double[][][] holes = regionJSON.getHoles();
		if (holes != null) {
			for (int i = 0; i < holes.length; i++) {
				for (int j = 0; j < holes[i].length; j++) {
					loecher.add(new Koordinate(holes[i][j][0], holes[i][j][1]));
				}
			}
		}
	}


    public static boolean istNachbar(Region region1, Region region2) {
        boolean istNachbar = false;
        for (int i = 0; i < region1.koordinaten.size(); i++) {
            Koordinate koordinate1 = region1.koordinaten.get(i);
            for (int j = 0; j < region2.koordinaten.size(); j++) {
                Koordinate koordinate2 = region2.koordinaten.get(j);
                if (koordinate1.istNahBei(koordinate2)) {
                    istNachbar = true;
                }
            }
        }
        return istNachbar;
    }


    public Set<Region> getGleichfarbigeNachbarn(FARBEN farbe){
		Set<Region> gleichfarbigeNachbarn = new HashSet<>();
		for (Region nachbarRegion : this.getNachbarn()){
			if (nachbarRegion.getFarbe() == farbe){
				gleichfarbigeNachbarn.add(nachbarRegion);
			}
		}
		return gleichfarbigeNachbarn;
	}


    public FARBEN getFarbe() {return this.farbe;}


    public Set<Region> getNachbarn (){
		return this.nachbarn;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public List<Koordinate> getKoordinaten() {
		return koordinaten;
	}


	public void setKoordinaten(List<Koordinate> koordinaten) {
		this.koordinaten = koordinaten;
	}


	public List<Koordinate> getLoecher() {
		return loecher;
	}


	public void setLoecher(List<Koordinate> loecher) {
		this.loecher = loecher;
	}


	public void setFarbe(FARBEN farbe) {
		this.farbe = farbe;
	}
}
