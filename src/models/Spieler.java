package models;

//import java.awt.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Created by Admin on 20/11/2016.
 */
public class Spieler {
    private FARBEN momentaneFarbe;
    private int spielstand; //punkte
    private boolean istJetztDran;
    private Region startRegion; //wozu wenn liste unten
    private Set<Region> gewonneneRegions;

    public Spieler(boolean istAlsErstesDran){
        this.istJetztDran = istAlsErstesDran;
        gewonneneRegions = new HashSet<>();
        spielstand = gewonneneRegions.size();


        //startRegion wird von FillerMap gesetzt, da dort die Liste<Region> ist
    }

    public boolean gewonneneRegionsNachbar(Region angeklickteRegion){
        for (Region gewonneneRegion : this.gewonneneRegions){
            for(Region nachbar : gewonneneRegion.getNachbarn()){
                if(angeklickteRegion==nachbar){
                    return true;
                }
            }
        }
        return false;

    }

    public void setStartRegion1(ArrayList<Region> regionList) {
        Region startRegion1 = regionList.get(0);
        this.startRegion = startRegion1;
    }

    public void setStartRegion(ArrayList<Region> regionList){
        Random zufall = new Random();
        int n = zufall.nextInt(regionList.size());
        this.startRegion = regionList.get(n);
    }

    public void setStartRegion2(ArrayList<Region> regionList) {  //was ist der unterschied von 1
        Region startRegion2 = regionList.get(regionList.size()-1); //hier kann exception auftreten
        this.startRegion = startRegion2;
    }
    public boolean getistJetztDran (){
        return this.istJetztDran;
    }
    public void setIstJetztDran(boolean istJetztDran){
        this.istJetztDran = istJetztDran;
    }
    public Region getStartRegion (){
        return this.startRegion;
    }
    public FARBEN getFarbe (){
        return this.momentaneFarbe;
    }

    public void setFarbe (FARBEN farbe){
        this.momentaneFarbe = farbe;
    }

    public int getSpielstand (){
        return this.spielstand;
    }
    public void setSpielstand(int spielstand){
        this.spielstand = spielstand;
    }
    public Set<Region> getGewonneneRegions(){
        return gewonneneRegions;
    }

}

