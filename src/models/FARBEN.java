package models;

import java.util.Random;

/**
 * Created by Admin on 29/11/2016.
 */
public enum FARBEN {
    A, B, C, D, E, F, G; //was bedeuten die namen

    public static FARBEN randomColor(){
        Random zufall = new Random();
        int n = zufall.nextInt(FARBEN.values().length);
        return FARBEN.values()[n];
    }

}
