package models;

/**
 * Created by Admin on 20/11/2016.
 */
public class Koordinate {
    private double x, y;
    private final static double EPSILON = 0.01;

    public Koordinate(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     *
     * @param andereKoordinate
     * @return true, wenn Koordinate und andere Koordinate Nachbarn sind
     */

    // Epsilon ist eine Variable für die genauere NAchbarbestimmung
    public boolean istNahBei(Koordinate andereKoordinate) {
        if (Math.abs(x- andereKoordinate.getX()) <= EPSILON) {
            if (Math.abs(y- andereKoordinate.getY()) <= EPSILON){
                return true;
            }
        }
        return false;
    }
    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
