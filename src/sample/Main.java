package sample;

import models.FillerMap;
import views.View;
import javafx.application.Application;
import javafx.stage.Stage;


public class Main extends Application {


    @Override
    public void start(Stage primaryStage) throws Exception{
        try {
            FillerMap fillerMap = new FillerMap();
            View view = new View(fillerMap, primaryStage);
        } catch (Exception e) {
			System.out.println("Fehler im Main.");
			e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
