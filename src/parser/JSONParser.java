package parser;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import models.Region;

/**
 * Created by Admin on 20/11/2016.
 */
//Fehler im JSON-Datei countries : line 6515 column 8 und Wiederholungen von Ländern (zumindest von manchen Namen)
public class JSONParser {
	/**
	 * Alle vorhandene Karten als Konstanten
	 */
	public static final String COUNTRIES = "jsondata/countries.json";
	public static final String HEXAGONS = "jsondata/hexagons.json";
	public static final String LANDKREISE = "jsondata/landkreise.json";
	public static final String RECTANGLES = "jsondata/rectangles.json";


	/**
	 *
	 * @param filename	Adresse der JSON-Datei, die abgelesen werden soll
	 * @return			Liste von Regionen
	 */
	public static List<Region> getRegions(String filename) {
		List<Region> regions = new ArrayList<>();
		List<RegionJSON> regionJSONs = getRegionsJSON(filename);

		for (RegionJSON rj : regionJSONs) {
			regions.add(new Region(rj));
		}

		return regions;
	}

	/**
	 *
	 * @param filename	Adresse der JSON-Datei, die abgelesen werden soll
	 * @return			Liste von urspruenglichen Regionen (die die gleichen Attribute haben wie Objekte in der JSON-Datei)
	 * @see				Gson Bibliothek
	 */
    private static List<RegionJSON> getRegionsJSON(String filename) {
        List<RegionJSON> countries = new ArrayList<>();

		try (InputStream is = ClassLoader.getSystemResourceAsStream(filename)) {
			Reader reader = new InputStreamReader(is);
			Gson gson = new Gson();
			countries = gson.fromJson(reader, new TypeToken<List<RegionJSON>>(){}.getType());
		} catch (IOException e) {
			e.printStackTrace();
		}

		return countries;
    }
}
